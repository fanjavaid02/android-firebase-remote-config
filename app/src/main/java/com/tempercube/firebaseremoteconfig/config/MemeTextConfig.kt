package com.tempercube.firebaseremoteconfig.config

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MemeTextConfig(
    @SerializedName("text_size") val textSize: Int,
    @SerializedName("text_color") val textColor: String
) : Serializable
