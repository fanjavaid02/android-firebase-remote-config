package com.tempercube.firebaseremoteconfig

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import androidx.core.widget.addTextChangedListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.tempercube.firebaseremoteconfig.config.MemeTextConfig
import com.tempercube.firebaseremoteconfig.databinding.ActivityMemeGeneratorBinding
import com.tempercube.firebaseremoteconfig.utils.RemoteConfigUtils

class MemeGeneratorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMemeGeneratorBinding

    private lateinit var mRemoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMemeGeneratorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        RemoteConfigUtils.getConfig(KEY_MEME_TEXT_CONFIG) {
            val gson = Gson()
            val config = gson.fromJson(it.asString(), MemeTextConfig::class.java)
            Log.d("MemeGeneratorActivity", "Success : $config")
            binding.memeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, config.textSize.toFloat())
            binding.memeText.setTextColor(Color.parseColor(config.textColor))
        }
    }

    override fun onResume() {
        super.onResume()

        binding.wordEdit.addTextChangedListener {
            binding.memeText.text = it.toString()
        }
    }

    companion object {
        const val KEY_MEME_TEXT_SIZE = "memeTextSize"
        const val KEY_MEME_TEXT_CONFIG = "memeTextConfig"
    }
}
