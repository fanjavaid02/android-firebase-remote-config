package com.tempercube.firebaseremoteconfig

import android.app.Application
import com.tempercube.firebaseremoteconfig.utils.RemoteConfigUtils

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        RemoteConfigUtils.init()
    }
}
