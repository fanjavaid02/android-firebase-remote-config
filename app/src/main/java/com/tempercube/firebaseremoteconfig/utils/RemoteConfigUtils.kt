package com.tempercube.firebaseremoteconfig.utils

import android.util.Log
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.tempercube.firebaseremoteconfig.BuildConfig

object RemoteConfigUtils {
    private val remoteConfig by lazy { Firebase.remoteConfig }

    fun init() {
        remoteConfigSettings {
            minimumFetchIntervalInSeconds = if (BuildConfig.DEBUG) 0 else 3600
        }.also {
            remoteConfig.setConfigSettingsAsync(it)
        }
    }

    fun getConfig(key: String, callback: (FirebaseRemoteConfigValue) -> Unit) {
        remoteConfig.fetchAndActivate().addOnCompleteListener {
            if (it.isSuccessful) {
                callback.invoke(remoteConfig.getValue(key))
            } else {
                Log.e(
                    RemoteConfigUtils::class.java.simpleName,
                    "Error : " + it.exception?.message
                )
            }
        }
    }
}
