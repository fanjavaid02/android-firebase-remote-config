package com.tempercube.firebaseremoteconfig

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tempercube.firebaseremoteconfig.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()
        binding.memeGeneratorButton.setOnClickListener {
            val memeIntent = Intent(this, MemeGeneratorActivity::class.java)
            startActivity(memeIntent)
        }
    }
}
